// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: api/rpc_node.proto

package api

import (
	common "chainmaker.org/chainmaker/pb-go/v2/common"
	config "chainmaker.org/chainmaker/pb-go/v2/config"
	context "context"
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

func init() { proto.RegisterFile("api/rpc_node.proto", fileDescriptor_ba278e4b8f6bb771) }

var fileDescriptor_ba278e4b8f6bb771 = []byte{
	// 384 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x84, 0x92, 0xcf, 0xaa, 0xd3, 0x40,
	0x14, 0xc6, 0x13, 0x0a, 0x8a, 0xe3, 0xaa, 0x63, 0xb5, 0x6d, 0x84, 0xa0, 0x5d, 0xa8, 0x1b, 0x13,
	0xa9, 0xe0, 0xc6, 0x5d, 0x2b, 0xb8, 0xa9, 0x15, 0x52, 0xff, 0x80, 0x20, 0x65, 0x32, 0x39, 0x4d,
	0x87, 0xa6, 0x39, 0xe3, 0x4c, 0x52, 0x7d, 0x0c, 0x1f, 0xcb, 0x65, 0x97, 0x2e, 0x2f, 0xed, 0x03,
	0xdc, 0x57, 0xb8, 0x34, 0xc9, 0xa4, 0xb9, 0xdc, 0x70, 0xef, 0x72, 0xbe, 0xdf, 0x77, 0xbe, 0xf9,
	0x66, 0x38, 0x84, 0x32, 0x29, 0x7c, 0x25, 0xf9, 0x32, 0xc5, 0x08, 0x3c, 0xa9, 0x30, 0x43, 0xda,
	0x61, 0x52, 0x38, 0x3d, 0x8e, 0xdb, 0x2d, 0xa6, 0xbe, 0x82, 0x5f, 0x39, 0xe8, 0xac, 0x44, 0xce,
	0xa3, 0x5a, 0xd5, 0x79, 0x62, 0xc4, 0x21, 0xc7, 0x74, 0x25, 0x62, 0x3f, 0x41, 0xce, 0x92, 0x65,
	0x79, 0xa8, 0x50, 0xbf, 0x46, 0xf1, 0x75, 0xe0, 0x56, 0x80, 0xaf, 0x99, 0x48, 0xb7, 0x6c, 0x03,
	0x6a, 0xa9, 0x41, 0xed, 0x40, 0x95, 0x7c, 0x7c, 0xd9, 0x21, 0xf7, 0x03, 0xc9, 0xe7, 0x18, 0x01,
	0x7d, 0x47, 0x1e, 0x2e, 0x20, 0x8d, 0x82, 0xb2, 0x09, 0xed, 0x7a, 0x65, 0x09, 0xef, 0xcb, 0x9f,
	0x4a, 0x72, 0x68, 0x53, 0xd2, 0x12, 0x53, 0x0d, 0x23, 0x8b, 0xbe, 0x27, 0x0f, 0x16, 0x79, 0xa8,
	0xb9, 0x12, 0x21, 0xb4, 0x4d, 0xf5, 0x8d, 0x54, 0xbb, 0x82, 0xe2, 0x59, 0x23, 0xeb, 0x8d, 0x4d,
	0xe7, 0xa4, 0xfb, 0x55, 0x46, 0x2c, 0x83, 0x0f, 0x10, 0xe6, 0xf1, 0xb4, 0x68, 0x4b, 0x1d, 0xaf,
	0x7a, 0x44, 0x43, 0x34, 0x69, 0x4f, 0x5b, 0x59, 0x5d, 0xe6, 0x33, 0x79, 0x12, 0xc0, 0x4a, 0x81,
	0x5e, 0xcf, 0x30, 0x9e, 0xc1, 0x0e, 0x12, 0x5d, 0x85, 0x0e, 0xcc, 0x60, 0x0d, 0x4c, 0xe4, 0xb0,
	0x85, 0xd4, 0x81, 0x3f, 0x49, 0xef, 0x23, 0x64, 0xd3, 0xd3, 0xff, 0x7d, 0x3a, 0xfd, 0xdf, 0x37,
	0x50, 0x5a, 0x60, 0x4a, 0x9f, 0x99, 0xa1, 0x1b, 0xc8, 0xc4, 0x3e, 0xbf, 0xc5, 0x51, 0xc7, 0x23,
	0x19, 0x4c, 0xd7, 0xc0, 0x37, 0x73, 0xf8, 0x3d, 0x49, 0x90, 0x6f, 0x0a, 0x6f, 0xd5, 0xf8, 0xe5,
	0x39, 0xa0, 0xdd, 0x61, 0x6e, 0x7a, 0x75, 0xb7, 0xd1, 0x5c, 0x38, 0xf9, 0xfe, 0xef, 0xe0, 0xda,
	0xfb, 0x83, 0x6b, 0x5f, 0x1c, 0x5c, 0xfb, 0xef, 0xd1, 0xb5, 0xf6, 0x47, 0xd7, 0xfa, 0x7f, 0x74,
	0x2d, 0xf2, 0x18, 0x55, 0xec, 0x9d, 0x17, 0xc5, 0x93, 0xa1, 0xc7, 0xa4, 0xf8, 0xf1, 0xa2, 0x21,
	0xa1, 0x6a, 0xae, 0x92, 0x2f, 0xc3, 0xd7, 0x31, 0xfa, 0xbb, 0xb1, 0xcf, 0xa4, 0x08, 0xef, 0x15,
	0x1b, 0xf5, 0xf6, 0x2a, 0x00, 0x00, 0xff, 0xff, 0x05, 0x8f, 0xbd, 0xbe, 0xeb, 0x02, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// RpcNodeClient is the client API for RpcNode service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type RpcNodeClient interface {
	// processing transaction message requests
	SendRequest(ctx context.Context, in *common.TxRequest, opts ...grpc.CallOption) (*common.TxResponse, error)
	// processing requests for message subscription
	Subscribe(ctx context.Context, in *common.TxRequest, opts ...grpc.CallOption) (RpcNode_SubscribeClient, error)
	// update debug status (development debugging)
	UpdateDebugConfig(ctx context.Context, in *config.DebugConfigRequest, opts ...grpc.CallOption) (*config.DebugConfigResponse, error)
	// refreshLogLevelsConfig
	RefreshLogLevelsConfig(ctx context.Context, in *config.LogLevelsRequest, opts ...grpc.CallOption) (*config.LogLevelsResponse, error)
	// get chainmaker version
	GetChainMakerVersion(ctx context.Context, in *config.ChainMakerVersionRequest, opts ...grpc.CallOption) (*config.ChainMakerVersionResponse, error)
	// check chain configuration and load new chain dynamically
	CheckNewBlockChainConfig(ctx context.Context, in *config.CheckNewBlockChainConfigRequest, opts ...grpc.CallOption) (*config.CheckNewBlockChainConfigResponse, error)
}

type rpcNodeClient struct {
	cc *grpc.ClientConn
}

func NewRpcNodeClient(cc *grpc.ClientConn) RpcNodeClient {
	return &rpcNodeClient{cc}
}

func (c *rpcNodeClient) SendRequest(ctx context.Context, in *common.TxRequest, opts ...grpc.CallOption) (*common.TxResponse, error) {
	out := new(common.TxResponse)
	err := c.cc.Invoke(ctx, "/api.RpcNode/SendRequest", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcNodeClient) Subscribe(ctx context.Context, in *common.TxRequest, opts ...grpc.CallOption) (RpcNode_SubscribeClient, error) {
	stream, err := c.cc.NewStream(ctx, &_RpcNode_serviceDesc.Streams[0], "/api.RpcNode/Subscribe", opts...)
	if err != nil {
		return nil, err
	}
	x := &rpcNodeSubscribeClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type RpcNode_SubscribeClient interface {
	Recv() (*common.SubscribeResult, error)
	grpc.ClientStream
}

type rpcNodeSubscribeClient struct {
	grpc.ClientStream
}

func (x *rpcNodeSubscribeClient) Recv() (*common.SubscribeResult, error) {
	m := new(common.SubscribeResult)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *rpcNodeClient) UpdateDebugConfig(ctx context.Context, in *config.DebugConfigRequest, opts ...grpc.CallOption) (*config.DebugConfigResponse, error) {
	out := new(config.DebugConfigResponse)
	err := c.cc.Invoke(ctx, "/api.RpcNode/UpdateDebugConfig", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcNodeClient) RefreshLogLevelsConfig(ctx context.Context, in *config.LogLevelsRequest, opts ...grpc.CallOption) (*config.LogLevelsResponse, error) {
	out := new(config.LogLevelsResponse)
	err := c.cc.Invoke(ctx, "/api.RpcNode/RefreshLogLevelsConfig", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcNodeClient) GetChainMakerVersion(ctx context.Context, in *config.ChainMakerVersionRequest, opts ...grpc.CallOption) (*config.ChainMakerVersionResponse, error) {
	out := new(config.ChainMakerVersionResponse)
	err := c.cc.Invoke(ctx, "/api.RpcNode/GetChainMakerVersion", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rpcNodeClient) CheckNewBlockChainConfig(ctx context.Context, in *config.CheckNewBlockChainConfigRequest, opts ...grpc.CallOption) (*config.CheckNewBlockChainConfigResponse, error) {
	out := new(config.CheckNewBlockChainConfigResponse)
	err := c.cc.Invoke(ctx, "/api.RpcNode/CheckNewBlockChainConfig", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RpcNodeServer is the server API for RpcNode service.
type RpcNodeServer interface {
	// processing transaction message requests
	SendRequest(context.Context, *common.TxRequest) (*common.TxResponse, error)
	// processing requests for message subscription
	Subscribe(*common.TxRequest, RpcNode_SubscribeServer) error
	// update debug status (development debugging)
	UpdateDebugConfig(context.Context, *config.DebugConfigRequest) (*config.DebugConfigResponse, error)
	// refreshLogLevelsConfig
	RefreshLogLevelsConfig(context.Context, *config.LogLevelsRequest) (*config.LogLevelsResponse, error)
	// get chainmaker version
	GetChainMakerVersion(context.Context, *config.ChainMakerVersionRequest) (*config.ChainMakerVersionResponse, error)
	// check chain configuration and load new chain dynamically
	CheckNewBlockChainConfig(context.Context, *config.CheckNewBlockChainConfigRequest) (*config.CheckNewBlockChainConfigResponse, error)
}

// UnimplementedRpcNodeServer can be embedded to have forward compatible implementations.
type UnimplementedRpcNodeServer struct {
}

func (*UnimplementedRpcNodeServer) SendRequest(ctx context.Context, req *common.TxRequest) (*common.TxResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SendRequest not implemented")
}
func (*UnimplementedRpcNodeServer) Subscribe(req *common.TxRequest, srv RpcNode_SubscribeServer) error {
	return status.Errorf(codes.Unimplemented, "method Subscribe not implemented")
}
func (*UnimplementedRpcNodeServer) UpdateDebugConfig(ctx context.Context, req *config.DebugConfigRequest) (*config.DebugConfigResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateDebugConfig not implemented")
}
func (*UnimplementedRpcNodeServer) RefreshLogLevelsConfig(ctx context.Context, req *config.LogLevelsRequest) (*config.LogLevelsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RefreshLogLevelsConfig not implemented")
}
func (*UnimplementedRpcNodeServer) GetChainMakerVersion(ctx context.Context, req *config.ChainMakerVersionRequest) (*config.ChainMakerVersionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetChainMakerVersion not implemented")
}
func (*UnimplementedRpcNodeServer) CheckNewBlockChainConfig(ctx context.Context, req *config.CheckNewBlockChainConfigRequest) (*config.CheckNewBlockChainConfigResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CheckNewBlockChainConfig not implemented")
}

func RegisterRpcNodeServer(s *grpc.Server, srv RpcNodeServer) {
	s.RegisterService(&_RpcNode_serviceDesc, srv)
}

func _RpcNode_SendRequest_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(common.TxRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcNodeServer).SendRequest(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.RpcNode/SendRequest",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcNodeServer).SendRequest(ctx, req.(*common.TxRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RpcNode_Subscribe_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(common.TxRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(RpcNodeServer).Subscribe(m, &rpcNodeSubscribeServer{stream})
}

type RpcNode_SubscribeServer interface {
	Send(*common.SubscribeResult) error
	grpc.ServerStream
}

type rpcNodeSubscribeServer struct {
	grpc.ServerStream
}

func (x *rpcNodeSubscribeServer) Send(m *common.SubscribeResult) error {
	return x.ServerStream.SendMsg(m)
}

func _RpcNode_UpdateDebugConfig_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(config.DebugConfigRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcNodeServer).UpdateDebugConfig(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.RpcNode/UpdateDebugConfig",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcNodeServer).UpdateDebugConfig(ctx, req.(*config.DebugConfigRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RpcNode_RefreshLogLevelsConfig_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(config.LogLevelsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcNodeServer).RefreshLogLevelsConfig(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.RpcNode/RefreshLogLevelsConfig",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcNodeServer).RefreshLogLevelsConfig(ctx, req.(*config.LogLevelsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RpcNode_GetChainMakerVersion_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(config.ChainMakerVersionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcNodeServer).GetChainMakerVersion(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.RpcNode/GetChainMakerVersion",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcNodeServer).GetChainMakerVersion(ctx, req.(*config.ChainMakerVersionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RpcNode_CheckNewBlockChainConfig_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(config.CheckNewBlockChainConfigRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RpcNodeServer).CheckNewBlockChainConfig(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.RpcNode/CheckNewBlockChainConfig",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RpcNodeServer).CheckNewBlockChainConfig(ctx, req.(*config.CheckNewBlockChainConfigRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _RpcNode_serviceDesc = grpc.ServiceDesc{
	ServiceName: "api.RpcNode",
	HandlerType: (*RpcNodeServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SendRequest",
			Handler:    _RpcNode_SendRequest_Handler,
		},
		{
			MethodName: "UpdateDebugConfig",
			Handler:    _RpcNode_UpdateDebugConfig_Handler,
		},
		{
			MethodName: "RefreshLogLevelsConfig",
			Handler:    _RpcNode_RefreshLogLevelsConfig_Handler,
		},
		{
			MethodName: "GetChainMakerVersion",
			Handler:    _RpcNode_GetChainMakerVersion_Handler,
		},
		{
			MethodName: "CheckNewBlockChainConfig",
			Handler:    _RpcNode_CheckNewBlockChainConfig_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Subscribe",
			Handler:       _RpcNode_Subscribe_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "api/rpc_node.proto",
}
